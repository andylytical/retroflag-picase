#!/bin/sh
# Install wrapper for manual install

# prompt for y/n input until a valid selection is given
ask() {
    prompt="$1 [y/n] "
    while true; do
        printf "$prompt"
        read -r key
        case "$key" in
            Y|y) return 0 ;;
            N|n) return 1 ;;
        esac
    done
}

# Run setup script
./setup.sh

echo "Installation complete."
if ask "Reboot?"; then
    shutdown -r now
fi
