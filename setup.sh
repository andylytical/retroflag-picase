#!/bin/sh
# Install script for RetroFlag Pi Case safe shutdown script

BASE=$( dirname $0 )
PI_CONFIG_FILE="/boot/config.txt"
INITD_DIR="/etc/init.d"
SYSTEMD_DIR="/etc/systemd/system"

INSTALL_DIR="/opt/retroflag-picase"  # hard coded in etc/* files


# re-mount "/" and "/boot" filesystems as read/write
remount_rw() {
    echo "Re-mounting readonly filesystems as read/write."
    mount -o remount,rw "/"
    mount -o remount,rw "/boot"
}

# enable UART (turn on case LED at beginning of boot)
enable_uart() {
    if grep -q "^enable_uart=1" "$PI_CONFIG_FILE"; then
        echo "UART already enabled."
    else
        echo "Enabling UART."
        sed -i '/^enable_uart=0/d' "$PI_CONFIG_FILE"  # delete existing
        echo >> "$PI_CONFIG_FILE"
        echo "# turn on case LED at beginning of boot" >> "$PI_CONFIG_FILE"
        echo "enable_uart=1" >> "$PI_CONFIG_FILE"
    fi
}

# install service prerequisites for OSMC
install_osmc_prereqs() {
    apt-get update
    apt-get install -y python-pip python-setuptools python-dev gcc
    pip install wheel
    pip install rpi.gpio
}

# install service script and specified version of the shutdown script
install_scripts() {
    echo "Installing scripts."
    os="$1"
    install -d "$INSTALL_DIR"
    install "$BASE/opt/service.py" "$INSTALL_DIR"
    install "$BASE/opt/shutdown.$os" "$INSTALL_DIR/shutdown.sh"
}

# install Systemd service file and enable/start service
install_systemd_service() {
    echo "Installing Systemd service."
    install -m 644 -T "$BASE/etc/picase.systemd" "$SYSTEMD_DIR/picase.service"
    systemctl daemon-reload
    systemctl enable picase.service
    systemctl start picase.service
}

# install Recalbox-style SysVinit service file and start service
install_recalbox_service() {
    echo "Installing Recalbox-style SysVinit service."
    service_file="$INITD_DIR/S30picase"
    install "./etc/picase.sysvinit" "$service_file"
    "$service_file" start
}


get_os() {
  # Try to determine OS
  local _os
  # try hostname first (most common with gaming builds)
  _os="${1:-$(uname -n | tr '[:upper:]' '[:lower:]')}"  # lowercase node name
  if echo $_os | grep -q 'retropie\|recalbox\|osmc\|libreelec' ; then
    : #pass
  else
    _os=$(lsb_release -i | awk '/Distributor ID/ {print $NF}' | tr '[:upper:]' '[:lower:]')
  fi
  echo $_os
}


# exit on error
set -e

# exit if not root
if [ $(id -u) -ne 0 ]; then
    echo "Install script must be ran as root."
    exit 1
fi

# perform install
os=$( get_os )
echo "GOT OS: '$os'"
case "$os" in
  raspbian|retropie)
    enable_uart
    install_scripts "$os"
    install_systemd_service
    ;;

  recalbox)
    remount_rw
    enable_uart
    install_scripts "$os"
    install_recalbox_service
    ;;

  osmc)
    enable_uart
    install_osmc_prereqs
    install_scripts "$os"
    install_systemd_service
    ;;

  libreelec)
    PI_CONFIG_FILE="/flash/config.txt"
    enable_uart
    install_scripts "$os"
  *)
    echo "Could not determine operating system."
    exit 1
    ;;
esac

echo "Install complete. OS must now be rebooted."
echo
